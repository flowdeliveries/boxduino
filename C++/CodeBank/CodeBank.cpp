
#include <iostream>
#include <string>
#include <stdint.h>  // for uint32_t, to get 32bit-wide rotates, regardless of the size of int.
#include <limits.h>  // for CHAR_BIT

using namespace std;


// ------------------------------------------------------------------------------------------------
// CLASS: ClassTuple
// ------------------------------------------------------------------------------------------------
class CodeTuple{
    int sCode, sFault, rCode, rFault;

    public:
    
        CodeTuple(int s1, int s2, int s3, int s4){
            sCode = s1;
            sFault = s2;
            rCode = s3;
            rFault = s4;
        }

        CodeTuple(){
            sCode = 0;
            sFault = 0;
            rCode = 0;
            rFault = 0;
        }

        int getSendCode(){ return sCode;}
        int getSendFault(){ return sFault;}
        int getReceiveFault(){ return rFault;}
        int getReceiveCode(){ return rCode;}

        string toString(){
                return "\t" + to_string(sCode)+" \t"+to_string(sFault)+" \t"+to_string(rCode)+" \t"+to_string(rFault);
        }
};

// ------------------------------------------------------------------------------------------------
// CLASS: CodeBank
// ------------------------------------------------------------------------------------------------
class CodeBank{
    unsigned int BOX;
    CodeTuple codeSet[64];

    /**
     * @description:    Method used to create the keycode for the tuple using the box number
     * @algorithm:      XOR
     * @return:         (index) XOR (rotated box number)
    */
    unsigned int generateSend(int index, int sbn){
        return index^sbn;
    }

    /**
     * @description:    Generates the remaining Codes (used to open the box initially)
     * @algorithm:      for each four digit code add a rotated box number and mod 10
     * @return:         4 digit integer
    */
    unsigned int generateCode(unsigned int sCode, unsigned int sbn){

        unsigned int digit[4] = {0, 0, 0, 0};
        unsigned int temp = sCode;

        int i=3;
        while(temp>0){
            digit[i] = temp%10;
            temp/=10;
            i--;
        }

        for(int i = 0; i<3; i++){
            digit[i] += sbn;
            digit[i] %= 10;
        }

        return digit[0]*1000 + digit[1]*100 + digit[2]*10 + digit[3];
    }

    public:
    /**
     * Creates 1000 unique code sets based on the box number.
    */
        void populateSet(){
            unsigned int temp[4];           // Used to temporarily store the values of the generated codes
            unsigned int sbn = BOX;         // Shifted box number 

            for(int i = 0; i< 64; i++){

                sbn = rotl32(sbn, 1);
                sbn %= 1000;

                temp[0] = generateSend(i, sbn);
                sbn = (sbn+53)*29;
                sbn %= 1000;


                temp[1] = generateCode(temp[0], sbn);
                sbn += 41;
                sbn %= 1000;


                temp[2] = generateCode(temp[1], sbn);
                sbn += 17;
                sbn %= 1000;

                temp[3] = generateCode(temp[2], sbn);

                codeSet[i] = CodeTuple(temp[0], temp[1], temp[2], temp[3]);
            }

        }

        // -------- Construtor
        CodeBank(int b){
            BOX = b;

            populateSet();
        }

        // -------- Search the code set
        int search(int key_code){

            for( int i = 0 ; i < 64; i ++){
                if(codeSet[i].getSendCode() == key_code){
                    return i;
                }
            }

            return 9999;

        }

        // -------- Print all tuples in code bank
        void printCodeSet(){

            for(int i = 0; i< 64; i++){
                cout << i << ") " << codeSet[i].toString() << "\n";
            }
        }
        // -------- Accessor 
        CodeTuple get(int i){
            return codeSet[i];
        }       

        // -------- Left Rotation
        uint32_t rotl32 (uint32_t value, unsigned int count) {
            const unsigned int mask = CHAR_BIT*sizeof(value) - 1;
            count &= mask;
            return (value << count) | (value >> (-count & mask));
        }

        


};



// ------------------------------------------------------------------------------------------------
// FUNCTION: Main
// ------------------------------------------------------------------------------------------------
int main(){

    unsigned int bnum = 5;
    unsigned int code;

    cout << "The box number is " << bnum <<  "\n";

    CodeBank bank = CodeBank(bnum);
    bank.printCodeSet();

    int send_code = 407;
    int receive_code = 1517;

    cout << "App sent me the code: " << send_code << "\n";
    int index = bank.search(send_code);
    cout << "Found a code at index: " << index << "\n";
    if(index != 9999)
    {
        CodeTuple codes = bank.get(index);
        cout << "The driver has arrived. Can I unlock the box with " << receive_code << "\n";
        if(receive_code == codes.getReceiveCode() )
            cout << "Yes!" << "\n";
        else
            cout << "No!" << "\n";
    }
    
    return 0;
}

