#include "Arduino.h"
#include "CodeBank.h"


// ------------------------------------------------------------------------------------------------
// CLASS: ClassTuple
// ------------------------------------------------------------------------------------------------

// Constructor
CodeTuple::CodeTuple( int s1, int s2, int s3, int s4)
{
    sCode = s1;
    sFault = s2;
    rCode = s3;
    rFault = s4;
}
CodeTuple::CodeTuple()
{
    sCode = 0;
    sFault = 0;
    rCode = 0;
    rFault = 0;
}

// Accessors
int CodeTuple::getSendCode(){ return sCode;}
int CodeTuple::getSendFault(){ return sFault;}
int CodeTuple::getReceiveFault(){ return rFault;}
int CodeTuple::getReceiveCode(){ return rCode;}

// To String
String CodeTuple::toString()
{
    return String(sCode)+" \t"+String(sFault)+" \t"+String(rCode)+" \t"+String(rFault);
}


// ------------------------------------------------------------------------------------------------
// CLASS: CodeBank
// ------------------------------------------------------------------------------------------------
/**
 * @description:    Method used to create the keycode for the tuple using the box number
 * @algorithm:      XOR
 * @return:         (index) XOR (rotated box number)
*/
unsigned int CodeBank::generateSend(int index, int sbn){
    return index^sbn;
}

/**
 * @description:    Generates the remaining Codes (used to open the box initially)
 * @algorithm:      for each four digit code add a rotated box number and mod 10
 * @return:         4 digit integer
*/
unsigned int CodeBank::generateCode(unsigned int sCode, unsigned int sbn){

    unsigned int digit[4] = {0, 0, 0, 0};
    unsigned int temp = sCode;

    int i=3;
    while(temp>0){
        digit[i] = temp%10;
        temp/=10;
        i--;
    }

    for(int i = 0; i<3; i++){
        digit[i] += sbn;
        digit[i] %= 10;
    }

    return digit[0]*1000 + digit[1]*100 + digit[2]*10 + digit[3];
}

/**
 * @description:    Creates 1000 unique code sets based on the box number.
*/
void CodeBank::populateSet(){
    unsigned int temp[4];           // Used to temporarily store the values of the generated codes
    unsigned int sbn = BOX;         // Shifted box number 

    for(int i = 0; i< 64; i++){

        sbn = rotl32(sbn, 1);
        sbn %= 1000;

        temp[0] = generateSend(i, sbn);
        sbn = (sbn+53)*29;
        sbn %= 1000;


        temp[1] = generateCode(temp[0], sbn);
        sbn += 41;
        sbn %= 1000;


        temp[2] = generateCode(temp[1], sbn);
        sbn += 17;
        sbn %= 1000;

        temp[3] = generateCode(temp[2], sbn);

        codeSet[i] = CodeTuple(temp[0], temp[1], temp[2], temp[3]);
    }

}

// -------- Construtor
CodeBank::CodeBank(int b){
    BOX = b;

    populateSet();
}

// -------- Accessor 
CodeTuple CodeBank::get(int i){
    return codeSet[i];
}

// -------- Search the code set
int CodeBank::search(int key_code){

    for( int i = 0 ; i < 64; i ++){
        if(codeSet[i].getSendCode() == key_code){
            return i;
        }
    }

    return 9999;

}

// -------- Left Rotation
uint32_t CodeBank::rotl32 (uint32_t value, unsigned int count) {
    const unsigned int mask = CHAR_BIT*sizeof(value) - 1;
    count &= mask;
    return (value << count) | (value >> (-count & mask));
}