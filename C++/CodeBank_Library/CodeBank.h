/*
    CodeBank.h - Library for generating and maintaining a small code base
    Flow, November 2018
*/

#ifndef CodeBank_h
#define CodeBank_h

#include "Arduino.h"
#include <stdint.h>  // for uint32_t, to get 32bit-wide rotates, regardless of the size of int.
#include <limits.h>  // for CHAR_BIT

// ------------------------------------------------------------------------------------------------
// CLASS: ClassTuple
// ------------------------------------------------------------------------------------------------
class CodeTuple{
    public:
        CodeTuple(int s1, int s2, int s3, int s4);
        CodeTuple();
        int getSendCode();
        int getSendFault();
        int getReceiveFault();
        int getReceiveCode();
        String toString();

    private:
        int sCode, sFault, rCode, rFault;


};

// ------------------------------------------------------------------------------------------------
// CLASS: CodeBank
// ------------------------------------------------------------------------------------------------
class CodeBank{

    public:
        CodeBank(int b);
        void populateSet();
        int search(int key_code);
        CodeTuple get(int i);
        uint32_t rotl32 (uint32_t value, unsigned int count);

    private:
        unsigned int BOX;
        CodeTuple codeSet[64];
        unsigned int generateSend(int index, int sbn);
        unsigned int generateCode(unsigned int sCode, unsigned int sbn);
};


#endif