#include <Keypad.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <CodeBank.h>


// -------- Setup the keyboard
const byte ROWS = 4;    //four rows
const byte COLS = 4;    //four columns
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte colPins[ROWS] = {5, 4, 3, 2};  //connect to the row pinouts of the keypad
byte rowPins[COLS] = {9, 8, 7, 6};  //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


// -------- Setup the LCD
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7);




// -------- Global scope variables
byte lockPin = 13; 
boolean lockPin_state;
String input_code = String();
CodeBank bank(5);
CodeTuple deliveryCodes;    // These are the set of codes found when the sender enters a correct code
enum states{
  IN_TRANSIT,
  BETWEEN_DELIVERIES
};
int delivery_state;


// -------- SETUP
void setup(){
    // ---- Setup serial coms
    Serial.begin(9600);

    // ---- Setup the GPIO
    pinMode(lockPin, OUTPUT);                 // Sets the digital pin as output.
    digitalWrite(lockPin, LOW);               // Turn the LED on.
    lockPin_state = digitalRead(lockPin);     // Store initial lock state. HIGH when unlocked.
    keypad.addEventListener(keypadEvent);     // Add an event listener for keypad

    // ---- Setup the LCD
    lcd.begin(16, 2);
    lcd.setBacklightPin(3, POSITIVE);
    lcd.setBacklight(HIGH);
    lcd.home();
    lcd.print("Welcome to FLOW");
    delay(1000);
    lcd.clear();
    lcd.print("TYPE PIN THEN #:");
    lcd.setCursor(0,1);

    // ---- Initial State
    delivery_state = BETWEEN_DELIVERIES;
}


// -------- MAIN LOOP
void loop(){
  
    char key = keypad.getKey();

    if (key) {
        if(key != '#' && key != '*')
            input_code.concat(key);
        Serial.println(input_code);
        lcd.setCursor(0,1);
        lcd.print("                ");
        lcd.setCursor(0,1);       
        lcd.print(input_code);
    }
}

// -------- Event Handler
// When the '#' key is pressed the code should be compared to the stored code
void keypadEvent(KeypadEvent key){

    if (key == '*' && keypad.getState()==RELEASED)  // Clear the input code
    {
        input_code = "";    // Clear the input code  
        lcd.setCursor(0,1);
        lcd.print("                ");
        lcd.setCursor(0,1);  
    }
    else if(key == '#' && keypad.getState()==RELEASED)  
    {
      Serial.print("Hash Pressed and the state is ");
      Serial.println(delivery_state, DEC);
      

        switch(delivery_state){

          case IN_TRANSIT:
              Serial.println("Code input while In Transit");
              Serial.println("Input code is " + input_code + ", and the reciever code is " +  deliveryCodes.getReceiveCode() );
              if(input_code.toInt() == deliveryCodes.getReceiveCode() )
              {
                  Serial.println("Receiver Unlocked");
                  lcd.print("Unlocked");              // To LCD
                  digitalWrite(lockPin, HIGH);        // Actually unlock device

                  delivery_state = BETWEEN_DELIVERIES;

                  countDown();
                  digitalWrite(lockPin, LOW);
              }
              else if(input_code.toInt() == deliveryCodes.getSendFault() )
              {
                  Serial.println("Sender Unlocked Again");
                  lcd.print("Unlocked");              // To LCD
                  digitalWrite(lockPin, HIGH);        // Actually unlock device

                  delivery_state = IN_TRANSIT;

                  countDown();
                  digitalWrite(lockPin, LOW);
              }
              else{
                  lcd.clear();
                  lcd.print("Incorrect Code");
                  delay(2000);
                  lcd.clear();
                  lcd.print("TYPE PIN THEN #:");
              }
              input_code = "";                        // Clear the input code   
              break;
              
          case BETWEEN_DELIVERIES:
              Serial.println("Code input while Between Deliveries");
              // ---- Is the code to open for the sender in the code bank?
              int index = bank.search(input_code.toInt());
              input_code = "";                        // Clear the input code   
              
              if(index != 9999){
                  Serial.println("Sender Unlocked");  // Log
                  lcd.print("Unlocked");              // To LCD
                  digitalWrite(lockPin, HIGH);        // Actually unlock device
                  deliveryCodes = bank.get(index);    // Fetch other codes
                  delivery_state = IN_TRANSIT;
      
                  countDown();
                  digitalWrite(lockPin, LOW);
              }
              else{
                  lcd.clear();
                  lcd.print("Incorrect Code");
                  delay(2000);
                  lcd.clear();
                  lcd.print("TYPE PIN THEN #:");
              } 
              break;          
              
        }//end switch
    }//end else
}// end funct



void countDown(){
    
    for(int i = 5; i > 0; i--){
        lcd.clear();
        lcd.print("Unlocked"); 
        lcd.setCursor(0,1);
        lcd.print(i);
        delay(1000);
    }
    
    lcd.clear();
    lcd.print("TYPE PIN THEN #:");

}
