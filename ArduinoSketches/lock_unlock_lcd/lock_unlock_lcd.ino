
#include <Keypad.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>

// -------- Setup the keyboard
const byte ROWS = 4;    //four rows
const byte COLS = 4;    //four columns
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte colPins[ROWS] = {5, 4, 3, 2};  //connect to the row pinouts of the keypad
byte rowPins[COLS] = {9, 8, 7, 6};  //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


// -------- Setup the LCD
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7);




// -------- Global scope variables
byte lockPin = 13; 
boolean lockPin_state;
String master_code = String(3568);    
String input_code = String();


// -------- SETUP
void setup(){
    Serial.begin(9600);
    pinMode(lockPin, OUTPUT);                 // Sets the digital pin as output.
    digitalWrite(lockPin, LOW);               // Turn the LED on.
    lockPin_state = digitalRead(lockPin);     // Store initial lock state. HIGH when unlocked.
    keypad.addEventListener(keypadEvent);     // Add an event listener for keypad

    // Setup the LCD
    lcd.begin(16, 2);
    lcd.setBacklightPin(3, POSITIVE);
    lcd.setBacklight(HIGH);
    lcd.home();
    lcd.print("Welcome to FLOW");
    delay(1000);
    lcd.clear();
    lcd.print("TYPE PIN THEN #:");
    lcd.setCursor(0,1);
}


// -------- MAIN LOOP
void loop(){
  
    char key = keypad.getKey();

    if (key) {
        if(key != '#' && key != '*')
            input_code.concat(key);
        Serial.println(input_code);
        lcd.setCursor(0,1);
        lcd.print("                ");
        lcd.setCursor(0,1);       
        lcd.print(input_code);
    }
}

// -------- Event Handler
// When the '#' key is pressed the code should be compared to the stored code
void keypadEvent(KeypadEvent key){
    if (key == '#' && keypad.getState()==RELEASED) {
        // ---- Comparing input to master code
        if(input_code.compareTo(master_code) == 0){
            input_code = "";                  // Clear the input code
            digitalWrite(lockPin, HIGH);      // Unlock
            lcd.clear();
            lcd.print("Unlocked"); 

            countDown();
            digitalWrite(lockPin, LOW);            
        }
        else{
          lcd.clear();
          lcd.print("Incorrect code");
          input_code = "";

          delay(2000);
          lcd.clear();
          lcd.print("TYPE PIN THEN #:");
          
        }
            
    }
    if (key == '*' && keypad.getState()==RELEASED) {
        input_code = "";    // Clear the input code  
        lcd.setCursor(0,1);
        lcd.print("                ");
        lcd.setCursor(0,1);  
    }
}


void countDown(){
    
    for(int i = 5; i > 0; i--){
        lcd.clear();
        lcd.print("Unlocked"); 
        lcd.setCursor(0,1);
        lcd.print(i);
        delay(1000);
    }
    
    lcd.clear();
    lcd.print("TYPE PIN THEN #:");

}
